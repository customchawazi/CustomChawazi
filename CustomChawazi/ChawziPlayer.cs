﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomChawazi.Models
{
    public class ChawziPlayer
    {
        public Color Color { get; set; }
        public PointF Position { get; set; }
        public ChawziPlayer(PointF position)
        {
            Random rnd = new Random();
            Color = Color.FromRgb(rnd.Next(256), rnd.Next(256), rnd.Next(256));
            Position = position;
        }
    }
}
