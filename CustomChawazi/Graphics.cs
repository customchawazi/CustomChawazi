﻿using CustomChawazi.Interfaces;
using CustomChawazi.Models;
using Color = Microsoft.Maui.Graphics.Color;
using PointF = Microsoft.Maui.Graphics.PointF;

namespace CustomChawazi
{
    public class GraphicsDrawable : IDrawable, IChawziGraphics
    {
        private const int WINNER_SHADOW_SIZE = 175;
        private const int PLAYER_SIZE_OUTER = 100;
        private const int PLAYER_SIZE_INNER = 75;
        private const int STROKE_SIZE = 8;
        private List<ChawziPlayer> _chawziPlayers;
        private bool _isGameOver;

        public GraphicsDrawable()
        {
            _chawziPlayers = [];
            _isGameOver = false;
        }

        public void Draw(ICanvas canvas, RectF dirtyRect)
        {
            foreach (var player in _chawziPlayers.ToList())
            {
                DrawPlayer(canvas, player);
            }
        }

        public void DrawPlayer(ICanvas canvas, ChawziPlayer player)
        {
            float halfOuterSize = PLAYER_SIZE_OUTER / 2;
            float halfInnerSize = PLAYER_SIZE_INNER / 2;

            float startX = player.Position.X - halfOuterSize;
            float startY = player.Position.Y - halfOuterSize;
            float innerStartX = player.Position.X - halfInnerSize;
            float innerStartY = player.Position.Y - halfInnerSize;

            if (_isGameOver)
            {
                // winner shade draw
                DrawWinningAnimation(canvas, player);
            }

            // outer ellipse frame
            canvas.StrokeColor = player.Color;
            canvas.StrokeSize = STROKE_SIZE;
            canvas.DrawEllipse(startX, startY, PLAYER_SIZE_OUTER, PLAYER_SIZE_OUTER);

            // inner ellpse 
            canvas.FillColor = player.Color;
            canvas.FillEllipse(innerStartX, innerStartY, PLAYER_SIZE_INNER, PLAYER_SIZE_INNER);
        }

        public void DrawWinningAnimation(ICanvas canvas, ChawziPlayer winner)
        {
            canvas.FillColor = winner.Color;
            canvas.FillRectangle(0, 0, 1000, 1000);

            float winnerShadowStartX = winner.Position.X - (WINNER_SHADOW_SIZE / 2);
            float winnerShadowStartY = winner.Position.Y - (WINNER_SHADOW_SIZE / 2);

            canvas.DrawEllipse(winnerShadowStartX, winnerShadowStartY, WINNER_SHADOW_SIZE, WINNER_SHADOW_SIZE);
            canvas.FillColor = Color.FromRgb(0, 0, 0);
            canvas.FillEllipse(winnerShadowStartX, winnerShadowStartY, WINNER_SHADOW_SIZE, WINNER_SHADOW_SIZE);
        }

        public void AddPlayer(ChawziPlayer newPlayer)
        {
            _chawziPlayers.Add(newPlayer);
        }

        public void MovePlayer(ChawziPlayer? needed, PointF location)
        {
            var player = _chawziPlayers.FirstOrDefault(player => player.Position.Equals(needed?.Position));
            if (player != null)
            {
                player.Position = location;
            }
        }

        public void DeletePlayer(ChawziPlayer? deletePlayer)
        {
            var playerToDelete = _chawziPlayers.FirstOrDefault(player => player.Position.Equals(deletePlayer?.Position));
            if (playerToDelete != null)
            {
                _chawziPlayers.Remove(playerToDelete);
            }
        }

        public void EndGame()
        {
            _chawziPlayers.RemoveRange(1, _chawziPlayers.Count() - 1);
            _isGameOver = true;
        }

        public void Restart()
        {
            _chawziPlayers.Clear();
            _isGameOver = false;
        }
    }
}
