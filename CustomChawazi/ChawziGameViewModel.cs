﻿using System.Timers;
using CommunityToolkit.Mvvm.ComponentModel;
using CustomChawazi.Interfaces;
using CustomChawazi.Models;

namespace CustomChawazi
{
    public partial class ChawziGameViewModel : ObservableObject, IChwaziGame
    {
        public IDrawable GameGraphics { get; set; }

        private List<ChawziPlayer> _activePlayers;
        private readonly System.Timers.Timer _gameTimer;
        private bool _isSuspended;
        public Action OnGameEnd { get; set; }

        private const int MIN_PLAYERS_COUNT = 2;
        private const int WINNER_WAIT_TIME = 3000;

        public ChawziGameViewModel(IDrawable graphics)
        {
            _activePlayers = new List<ChawziPlayer>();
            GameGraphics = graphics;
            _isSuspended = false;
            _gameTimer = new System.Timers.Timer
            {
                Interval = WINNER_WAIT_TIME
            };
            _gameTimer.Elapsed += new ElapsedEventHandler(EndGame);
            OnGameEnd = () => { };
        }

        public void SetEndGameAction(Action endGame)
        {
            OnGameEnd += endGame;
        }

        public void AddPlayer(PointF location)
        {
            if (_isSuspended)
                return;
            var isPlayerExist = _activePlayers.FirstOrDefault(player => player.Position.Equals(location));
            if (isPlayerExist == null)
            {
                // player is not exist
                var newPlayer = new ChawziPlayer(location);
                _activePlayers.Add(newPlayer);
                (GameGraphics as GraphicsDrawable)?.AddPlayer(newPlayer);
                Task.Run(() => SoundPlayer.Play(SoundType.Add));
            }

            if (_activePlayers.Count >= MIN_PLAYERS_COUNT)
            {
                if (_gameTimer.Enabled)
                {
                    _gameTimer.Stop();
                }
                _gameTimer.Start();
            }
        }

        public void Move(PointF location)
        {
            if (_isSuspended)
                return;
            var nearestPlayer = FindNearestPlayer(location);
            (GameGraphics as GraphicsDrawable)?.MovePlayer(nearestPlayer, location);
        }

        public void DeletePlayer(PointF location)
        {
            if (_isSuspended)
                return;
            var nearestPlayer = FindNearestPlayer(location);
            (GameGraphics as GraphicsDrawable)?.DeletePlayer(nearestPlayer);
            if (nearestPlayer != null)
            {
                _activePlayers.Remove(nearestPlayer);
                Task.Run(() => 
                SoundPlayer.Play(SoundType.Remove));
                if (_activePlayers.Count <= MIN_PLAYERS_COUNT && _gameTimer.Enabled)
                {
                    _gameTimer.Stop();
                }
            }
        }


        public void DeleteInactivePlayers(IEnumerable<PointF> currentActiveLocations)
        {
            if (!_isSuspended)
            {
                var playersToRemove = _activePlayers.Where(player => currentActiveLocations.FirstOrDefault(activePosition => activePosition.Equals(player.Position)) == default);
                foreach (var player in playersToRemove)
                {
                    (GameGraphics as GraphicsDrawable)?.DeletePlayer(player);
                    Task.Run(() => SoundPlayer.Play(SoundType.Remove));

                }
                _activePlayers = _activePlayers.Except(playersToRemove).ToList();
            }
        }

        public void EndGame(object? sender, ElapsedEventArgs eventArgs)
        {
            Task.Run(() => 
            SoundPlayer.Play(SoundType.End));
            _isSuspended = true;
            _gameTimer.Stop();
            _activePlayers.RemoveRange(1, _activePlayers.Count() - 1);
            (GameGraphics as GraphicsDrawable)?.EndGame();
            OnGameEnd?.Invoke();
            Thread.Sleep(2000);
            Restart();
        }

        public void Restart()
        {
            _isSuspended = false;
            _activePlayers.Clear();
            (GameGraphics as GraphicsDrawable)?.Restart();
            OnGameEnd?.Invoke();
        }
        private ChawziPlayer? FindNearestPlayer(PointF location)
        {
            return _activePlayers.OrderBy(player => Distance(location, player.Position)).FirstOrDefault();
        }

        private static double Distance(PointF p1, PointF p2)
        {
            var sum = Math.Abs((Math.Pow(p1.X - p2.X, 2) + Math.Pow(p1.Y - p2.Y, 2)));
            return Math.Sqrt(sum);
        }

    }
}