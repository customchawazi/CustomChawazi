﻿using CustomChawazi.Interfaces;

namespace CustomChawazi
{
    public partial class MainPage : ContentPage
    {
        private readonly IChwaziGame _gameHandler;
        public MainPage(IChwaziGame vm)
        {
            _gameHandler = vm;
            BindingContext = vm;
            _gameHandler.SetEndGameAction(UpdateCanvas);
            SetCanvasAnimation();
            InitializeComponent();
        }

        private void Canvas_MoveHoverInteraction(object? sender, TouchEventArgs e)
        {
            foreach (var point in e.Touches)
            {
                _gameHandler.AddPlayer(point);
            }
            _gameHandler.DeleteInactivePlayers(e.Touches);
            UpdateCanvas();
        }


        private void Canvas_DragInteraction(object sender, TouchEventArgs e)
        {
            foreach (var point in e.Touches)
            {
                _gameHandler.Move(point);
            }
            _gameHandler.DeleteInactivePlayers(e.Touches);
            UpdateCanvas();

        }

        private void canvas_EndInteraction(object sender, TouchEventArgs e)
        {
            if (e.Touches.Length == 1)
            {
                var point = e.Touches.First();   
                _gameHandler.DeletePlayer(point);
                UpdateCanvas();
            }
        }

        private void UpdateCanvas()
        {
            canvas.Invalidate();
        }

        private void SetCanvasAnimation()
        {
            var parentAnimation = new Animation();
            var scaleUpAnimation = new Animation(v =>
            {
                canvas.Scale = v;
                canvas.Scale = v;
            }, 1, 1.10);
            var scaleDownAnimation = new Animation(v =>
            {
                canvas.Scale = v;
                canvas.Scale = v;
            }, 1.10, 1);
            parentAnimation.Add(0, 0.5, scaleUpAnimation);
            parentAnimation.Add(0.5, 1, scaleDownAnimation);
            parentAnimation.Commit(this, "animation", 16, 1500, null, (v, c) => { }, () => true);
        }
    }
}
