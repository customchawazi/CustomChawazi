﻿using Plugin.Maui.Audio;

namespace CustomChawazi
{
    public static class SoundPlayer
    {
        public static async Task Play(SoundType type)
        {
            var soundName = GetSoundName(type);
            var audioPlayer = AudioManager.Current.CreatePlayer(await FileSystem.OpenAppPackageFileAsync(soundName));
            audioPlayer.Play();
        }

        private static string GetSoundName(SoundType type)
        {
            return type switch
            {
                SoundType.Add => "addedPlayer.wav",
                SoundType.Remove => "removePlayer.wav",
                SoundType.End => "endGame.wav",
                _ => string.Empty,
            };
        }
    }

    public enum SoundType
    {
        Add,
        Remove,
        End
    }
}
