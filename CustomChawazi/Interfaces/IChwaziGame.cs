﻿using System.Timers;

namespace CustomChawazi.Interfaces
{
    public interface IChwaziGame
    {
        public void AddPlayer(PointF location);
        public void Move(PointF location);
        public void DeletePlayer(PointF location);
        public void EndGame(object? sender, ElapsedEventArgs eventArgs);
        public void SetEndGameAction(Action endGame);
        public void DeleteInactivePlayers(IEnumerable<PointF> currentActiveLocations);
        public void Restart();
    }
}
