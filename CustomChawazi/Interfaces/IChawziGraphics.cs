﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomChawazi.Interfaces
{
    internal interface IChawziGraphics
    {
        public void Draw(ICanvas canvas, RectF dirtyRect);
    }
}
